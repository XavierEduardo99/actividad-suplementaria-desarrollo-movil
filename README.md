# # Actividad Suplementaria 2.2 - Desarrollo Móvil  (Javier Vintimilla)

Actividad suplementaria: Desarrolle una aplicación en Flutter el cual permita realizar una presentación de galería de imágenes, realice un informe de lo desarrollado con capturas de pantalla, código y publíquelo en el foro
## Versiones de software utilizadas:
- Flutter 3.16.5
- Android Studio Giraffe | 2022.3.1 Patch 3
- Runtime version: 17.0.6+0-b2043.56-10027231 amd64
- VM: OpenJDK 64-Bit Server VM by JetBrains s.r.o.
- Android SDK: 34


Este proyecto es una demostración de una galería utilizando el framework de desarrollo Flutter.
