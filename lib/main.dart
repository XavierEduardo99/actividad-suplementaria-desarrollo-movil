import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Actividad Suplementaria 2 - Javier Vintimilla',
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.blueAccent,
      ),

      themeMode: ThemeMode.dark,

      home: const MyGallery(),
    );
  }
}

class MyGallery extends StatefulWidget {
  const MyGallery({super.key});

  @override
  _MyGalleryState createState() => _MyGalleryState();
}

class _MyGalleryState extends State<MyGallery> {
  List<String> urlFotos = [
    'https://cdn.pixabay.com/photo/2017/03/28/22/55/night-photograph-2183637_960_720.jpg',
    'https://cdn.pixabay.com/photo/2018/02/27/23/03/autumn-3186876_960_720.jpg',
    'https://cdn.pixabay.com/photo/2018/01/14/23/12/nature-3082832_960_720.jpg',
    'https://cdn.pixabay.com/photo/2016/11/29/05/45/astronomy-1867616_960_720.jpg',
	  'https://cdn.pixabay.com/photo/2017/06/09/11/55/cuba-2386659_960_720.jpg',
    'https://cdn.pixabay.com/photo/2019/08/26/03/35/cathedral-4430675_960_720.jpg',
    'https://cdn.pixabay.com/photo/2013/03/09/18/55/lama-91978_960_720.jpg',
	  'https://cdn.pixabay.com/photo/2016/04/22/22/50/oldtimer-1346830_960_720.jpg',
    'https://cdn.pixabay.com/photo/2013/02/10/00/02/blue-footed-boobie-79839_960_720.jpg',
    'https://cdn.pixabay.com/photo/2021/08/04/13/06/software-developer-6521720_960_720.jpg'
  ];

  void _navigateToFullScreenImage(String urlFoto) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => FullScreenImage(urlFotoAmplificar: urlFoto),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Actividad Suplementaria 2'),
      ),
      body: Stack(
        children: [
          GridView.builder(
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 8.0,
              mainAxisSpacing: 8.0,
            ),
            itemCount: urlFotos.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  _navigateToFullScreenImage(urlFotos[index]);
                },
                child: Image.network(
                  urlFotos[index],
                  fit: BoxFit.cover,
                ),
              );
            },
          ),
          Positioned(
            bottom: 8.0,
            left: 8.0,
            child: Container(
              padding: const EdgeInsets.all(8.0),
              child: const Text(
                'Realizado por: Javier Vintimilla (100-RED)',
                style: TextStyle(
                  color: Colors.white70,
                  fontSize: 12.0,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class FullScreenImage extends StatelessWidget {
  final String urlFotoAmplificar;

  const FullScreenImage({super.key, required this.urlFotoAmplificar});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Image.network(
          urlFotoAmplificar,
          fit: BoxFit.contain,
        ),
      ),
    );
  }
}

// Realizado por Javier Vintimilla el 23 de enero de 2024